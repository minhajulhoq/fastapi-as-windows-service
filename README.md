# FastAPI as Windows Service

This is a small FastAPI project to run as a service on Windows platform. This basic level project will make you understand how to run a FastAPI as a windows service.

## Getting started

Let's start the project with step by step guide.

## **Steps**

1. Download and install **Python** on windows.
2. Download **NSSM** on windows and set the **`nssm.exe`** file path to system environment variables.
3. Install **FastAPI** from **`cmd`** as admin with **`pip install fastapi`**
4. Install **Uvicorn** **`pip install uvicorn`**
5. Open VSCode(recommended) to start working with the project.
6. Create a folder named **testapi**(anyname).
7. Create a python file named **`main.py`** inside that folder.
8. Insert or write this below code in **`main.py`** -

```
from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional

import uvicorn

app = FastAPI()

fakedb = []

class Course(BaseModel):
    id: int
    name: str
    price: float
    is_early_bird: Optional[bool] = None

@app.get("/")
def read_root():
    return {"greetings": "Welocome to Shubbak"}

@app.get("/courses")
def get_courses():
    return fakedb

@app.get("/courses/{course_id}")
def get_a_course(course_id: int):
    course = course_id - 1
    return fakedb[course]


@app.post("/course")
def add_course(course: Course):
    fakedb.append(course.dict())
    return fakedb[-1]
@app.delete("/courses/{course_id}")
def delete_course(course_id:int):
    fakedb.pop(course_id-1)
    return{"task": "deletion success"}


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=5000, reload=True)
```

So, you don't have to understand this whole code block. Let's just try to understand what we are doing here.

Here we are trying to run the **FastAPI with Uvicorn** (an ASGI web server implementation for Python).
We can run the uvicorn from command line for the FastAPI but to run the FastAPI as a windows service, we have to deploy uvicorn programatically, basically run uvicorn directly from the Python script in **`main.py`**.


- In the code first we are importing the **uvicorn** with **`import uvicorn`**
- Then we are adding the configuration to run uvicorn at the bottom of the code-

```
if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=5000, reload=True)
```
You can configure this on your set of preferences.

9. Run **`cmd`** as admin and try installing service with **`nssm install FastAPI`** (You can put any service name instead of **`FastAPI`**)
##### A nssm service window will appear.

- Set **`Path`** to the **`python.exe`** path directory
- Set **`Startup directory`** to the **`main.py`** folder **`testapi`** path drectory
- Set **`Arguements`** to **`main.py`** and click **`Start service`**

##### Or we can install nssm with **`cmd`** -

- **`nssm install [Service Name] [python.exe PATH] main.py`**
- **`nssm set AppDirectory [Your App Folder PATH]`**
- **`nssm start [Service Name]`**

10. You can start the service with **`nssm start FastAPI`** too. 
11. Check the service status **`nssm status FastAPI`**

If the status shows the service is running, then check in the browser with **`host ip`** and **`port number`**. You can check the service status in windows **services** application also.

## Congrats! You are now running your FastAPI as a Windows Service.


