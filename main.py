from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional

import uvicorn

app = FastAPI()

fakedb = []

class Course(BaseModel):
    id: int
    name: str
    price: float
    is_early_bird: Optional[bool] = None

@app.get("/")
def read_root():
    return {"greetings": "Welocome to FastAPI"}

@app.get("/courses")
def get_courses():
    return fakedb

@app.get("/courses/{course_id}")
def get_a_course(course_id: int):
    course = course_id - 1
    return fakedb[course]


@app.post("/course")
def add_course(course: Course):
    fakedb.append(course.dict())
    return fakedb[-1]
@app.delete("/courses/{course_id}")
def delete_course(course_id:int):
    fakedb.pop(course_id-1)
    return{"task": "deletion success"}


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=5000, reload=True)
